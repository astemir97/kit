from flask import Flask, render_template, redirect, flash, Blueprint, request, url_for
from flask_login import login_required
from cuba import db
from cuba.models import Todo

main = Blueprint('main', __name__)


# Create your views here.

# -------------------------General(Dashboards,Widgets & Layout)---------------------------------------

# ---------------Main

@main.route('/')
@login_required
def index():
    return redirect('dashboard')


@main.route('/dashboard')
@login_required
def dashboard():
    context = {"breadcrumb": {"parent": "Dashboard", "jsFunction": 'startTime()'}}
    return render_template("general/dashboard/default/index.html", **context)


# ---------------------Booking
@main.route('/bookings')
@login_required
def projects():
    context = {"breadcrumb": {"parent": "bookings", "child": "Bookings"}}
    return render_template("applications/projects/projects-list/projects.html", **context)


@main.route('/bookingcreate')
@login_required
def projectcreate():
    context = {"breadcrumb": {"parent": "bookings", "child": "Booking Create"}}
    return render_template("applications/projects/projectcreate/projectcreate.html", **context)


# --------------------ecommerce
@main.route('/product_cards')
@login_required
def product_cards():
    context = {"breadcrumb": {"parent": "Ecommerce", "child": "Product"}}
    return render_template("applications/ecommerce/product/product.html", **context)


@main.route('/product_page')
@login_required
def product_page():
    context = {"breadcrumb": {"parent": "Bookings", "child": "Hotel Page"}}
    return render_template("applications/ecommerce/product-page/product-page.html", **context)


@main.route('/list_products')
@login_required
def list_products():
    context = {"breadcrumb": {"parent": "Ecommerce", "child": "Visa Saudi"}}
    return render_template("applications/ecommerce/list-products/list-products.html", **context)


@main.route('/guests')
@login_required
def support_ticket():
    context = {"breadcrumb": {"parent": "Guests"}}
    return render_template("miscellaneous/support-ticket/support-ticket.html", **context)


@main.route('/settings')
@login_required
def settings():
    context = {"breadcrumb": {"parent": "Users", "child": "Settings"}}
    return render_template("applications/settings/settings-profile/settings-profile.html", **context)


@main.route('/chat_app')
# --------------------------------chat
@login_required
def chat_app():
    context = {"breadcrumb": {"parent": "Chat", "child": "Chat App"}}
    return render_template("applications/chat/chat/chat.html", **context)


@main.route('/edit_profile')
def edit_profile():
    context = {"breadcrumb": {"parent": "Users", "child": "User Profile"}}
    return render_template("applications/user/edit-profile/edit-profile.html", **context)


# ------------------------bookmark
@main.route('/bookmark')
@login_required
def bookmark():
    context = {"breadcrumb": {"parent": "Apps", "child": "Bookmark"}}
    return render_template("applications/bookmark/bookmark.html", **context)


#################################################################################

@main.route('/avatars')
@login_required
def avatars():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Avatars"}}
    return render_template('components/ui-kits/avatars.html', **context)


@main.route('/helper')
@login_required
def helper():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Helper Classes"}}
    return render_template('components/ui-kits/helper.html', **context)


@main.route('/grid')
@login_required
def grid():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "grid"}}
    return render_template('components/ui-kits/grid.html', **context)


@main.route('/tagpill')
@login_required
def tagpill():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Tag & Pills"}}
    return render_template('components/ui-kits/tag-pills.html', **context)


@main.route('/progressbar')
@login_required
def progressbar():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Progress"}}
    return render_template('components/ui-kits/progressbar.html', **context)


@main.route('/modal')
@login_required
def modal():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "modal"}}
    return render_template('components/ui-kits/modal.html', **context)


@main.route('/alert')
@login_required
def alert():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "alert"}}
    return render_template('components/ui-kits/alert.html', **context)


@main.route('/popover')
@login_required
def popover():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "popover"}}
    return render_template('components/ui-kits/popover.html', **context)


@main.route('/tooltip')
@login_required
def tooltip():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "tooltip"}}
    return render_template('components/ui-kits/tooltip.html', **context)


@main.route('/spiners')
@login_required
def spiners():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Spinners"}}
    return render_template('components/ui-kits/spiners.html', **context)


@main.route('/dropdown')
@login_required
def dropdown():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "dropdown"}}
    return render_template('components/ui-kits/dropdown.html', **context)


@main.route('/bootstraptab')
@login_required
def bootstraptab():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Bootstrap Tabs"}}
    return render_template('components/ui-kits/bootstraptab.html', **context)


@main.route('/lists')
@login_required
def lists():
    context = {"breadcrumb": {"parent": "Ui Kits", "child": "Lists"}}
    return render_template('components/ui-kits/lists.html', **context)


@login_required
@main.route('/tree')
def tree():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Tree View"}}
    return render_template('components/bonus-ui/tree.html', **context)


@login_required
@main.route('/bootstrapnotify')
def bootstrapnotify():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Bootstrap Notify"}}
    return render_template('components/bonus-ui/bootstrapnotify.html', **context)


@main.route('/rating')
@login_required
def rating():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "rating"}}
    return render_template('components/bonus-ui/rating.html', **context)


@main.route('/dropzone')
@login_required
def dropzone():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "dropzone"}}
    return render_template('components/bonus-ui/dropzone.html', **context)


@main.route('/tour')
@login_required
def tour():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "tour"}}
    return render_template('components/bonus-ui/tour.html', **context)


@main.route('/sweetalert2')
@login_required
def sweetalert2():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Sweet Alert"}}
    return render_template('components/bonus-ui/sweetalert.html', **context)


@main.route('/animatedmodal')
@login_required
def animatedmodal():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Animated Modal"}}
    return render_template('components/bonus-ui/animatedmodal.html', **context)


@main.route('/ribbons')
@login_required
def ribbons():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Ribbons"}}
    return render_template('components/bonus-ui/ribbons.html', **context)


@main.route('/pagination')
@login_required
def pagination():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Pagination"}}
    return render_template('components/bonus-ui/pagination.html', **context)


@main.route('/steps')
@login_required
def steps():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Steps"}}
    return render_template('components/bonus-ui/steps.html', **context)


@main.route('/breadcrumb')
@login_required
def breadcrumb():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Breadcrumb"}}
    return render_template('components/bonus-ui/breadcrumb.html', **context)


@main.route('/rangeslider')
@login_required
def rangeslider():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Range Slider  "}}
    return render_template('components/bonus-ui/rangeslider.html', **context)


@main.route('/imagecropper')
@login_required
def imagecropper():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Image Cropper"}}
    return render_template('components/bonus-ui/imagecropper.html', **context)


@main.route('/sticky')
@login_required
def sticky():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Sticky"}}
    return render_template('components/bonus-ui/sticky.html', **context)


@main.route('/basiccard')
@login_required
def basiccard():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Basic Card"}}
    return render_template('components/bonus-ui/basiccard.html', **context)


@main.route('/creativecard')
@login_required
def creativecard():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Creative Card"}}
    return render_template('components/bonus-ui/creativecard.html', **context)


@main.route('/tabbedcard')
@login_required
def tabbedcard():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Tabbed Card"}}
    return render_template('components/bonus-ui/tabbedcard.html', **context)


@main.route('/draggablecard')
@login_required
def draggablecard():
    context = {"breadcrumb": {"parent": "Bonus Ui", "child": "Draggable Card"}}
    return render_template('components/bonus-ui/draggablecard.html', **context)


@main.route('/formbuilder2')
@login_required
def formbuilder2():
    context = {"breadcrumb": {"parent": "Builders", "child": "Process Booking"}}
    return render_template('components/builders/formbuilder2.html', **context)


# ---------------------------------Animation
@main.route('/animate')
@login_required
def animate():
    context = {"breadcrumb": {"parent": "Animation", "child": "Animate"}}
    return render_template('components/animation/animate.html', **context)


@main.route('/scrollreval')
@login_required
def scrollreval():
    context = {"breadcrumb": {"parent": "Animation", "child": "scroll reveal"}}
    return render_template('components/animation/scrollreval.html', **context)


# -------------------------sample-page
@main.route('/sample_page')
@login_required
def sample_page():
    context = {"breadcrumb": {"parent": "Pages", "child": "Sample Page"}}
    return render_template('pages/sample-page/sample-page.html', **context)


# --------------------------internationalization
@main.route('/internationalization')
@login_required
def internationalization():
    context = {"breadcrumb": {"parent": "Pages", "child": "Internationalization"}}
    return render_template('pages/internationalization/internationalization.html', **context)


# ----------------------------------Authentication

@main.route('/login_one')
@login_required
def login_one():
    return render_template('pages/others/authentication/login-one/login-one.html')


@main.route('/login_simple')
@login_required
def login_simple():
    return render_template('pages/others/authentication/login/login.html')


@main.route('/login_two')
@login_required
def login_two():
    return render_template('pages/others/authentication/login-two/login-two.html')


@main.route('/login_bs_validation')
@login_required
def login_bs_validation():
    return render_template('pages/others/authentication/login-bs-validation/login-bs-validation.html')


@main.route('/login_tt_validation')
@login_required
def login_tt_validation():
    return render_template('pages/others/authentication/login-bs-tt-validation/login-bs-tt-validation.html')


@main.route('/login_validation')
@login_required
def login_validation():
    return render_template('pages/others/authentication/login-sa-validation/login-sa-validation.html')


@main.route('/sign_up')
@login_required
def sign_up():
    return render_template('pages/others/authentication/sign-up/sign-up.html')


@main.route('/sign_one')
@login_required
def sign_one():
    return render_template('pages/others/authentication/sign-one/sign-up-one.html')


@main.route('/sign_two')
@login_required
def sign_two():
    return render_template('pages/others/authentication/sign-two/sign-up-two.html')


@main.route('/sign_wizard')
@login_required
def sign_wizard():
    return render_template('pages/others/authentication/sign-up-wizard/sign-up-wizard.html')


@main.route('/unlock')
@login_required
def unlock():
    return render_template('pages/others/authentication/unlock/unlock.html')


@main.route('/forget_password')
@login_required
def forget_password():
    return render_template('pages/others/authentication/forget-password/forget-password.html')


@main.route('/reset_password')
@login_required
def reset_password():
    return render_template('pages/others/authentication/reset-password/reset-password.html')


@main.route('/maintenance')
@login_required
def maintenance():
    return render_template('pages/others/authentication/maintenance/maintenance.html')


# ------------------------------------------Miscellaneous----------------- -------------------------

# --------------------------------------gallery
@main.route('/gallery_grid')
@login_required
def gallery_grid():
    context = {"breadcrumb": {"parent": "Gallery", "child": "Gallery"}}
    return render_template('miscellaneous/gallery/gallery-grid/gallery.html', **context)


@login_required
@main.route('/grid_description')
def grid_description():
    context = {"breadcrumb": {"parent": "Gallery", "child": "Gallery Grid With Description"}}
    return render_template('miscellaneous/gallery/gallery-grid-desc/gallery-with-description.html', **context)


@login_required
@main.route('/masonry_gallery')
def masonry_gallery():
    context = {"breadcrumb": {"parent": "Gallery", "child": "Masonry Gallery"}}
    return render_template('miscellaneous/gallery/masonry-gallery/gallery-masonry.html', **context)


@main.route('/masonry_disc')
@login_required
def masonry_disc():
    context = {"breadcrumb": {"parent": "Gallery", "child": "Masonry Gallery With Description"}}
    return render_template('miscellaneous/gallery/masonry-with-desc/masonry-gallery-with-disc.html', **context)


@main.route('/hover')
@login_required
def hover():
    context = {"breadcrumb": {"parent": "Gallery", "child": "Image Hover Effects"}}
    return render_template('miscellaneous/gallery/hover-effects/gallery-hover.html', **context)


# --------------------------------------faq
@main.route('/FAQ')
@login_required
def FAQ():
    context = {"breadcrumb": {"parent": "FAQ", "child": "FAQ"}}
    return render_template('miscellaneous/FAQ/faq.html', **context)


# ---------------------------------job serach
@main.route('/job_cards')
@login_required
def job_cards():
    context = {"breadcrumb": {"parent": "Job Search", "child": "Cards View"}}
    return render_template('miscellaneous/job-search/cards-view/job-cards-view.html', **context)


@login_required
@main.route('/job_list')
def job_list():
    context = {"breadcrumb": {"parent": "Job Search", "child": "List View"}}
    return render_template('miscellaneous/job-search/list-view/job-list-view.html', **context)


# ----------------------------knowledgeUi Kits
@main.route('/knowledgebase')
@login_required
def knowledgebase():
    context = {"breadcrumb": {"parent": "KnowledgeUi Kits", "child": "KnowledgeUi Kits"}}
    return render_template('miscellaneous/knowledgebase/knowledgebase.html', **context)


# ---------------------------------------------------------------------------------------

@main.route('/to_do_database')
@login_required
def to_do_database():
    todos = Todo.query.all()
    allTasksComplete = True

    for todo in todos:
        if not todo.completed:
            allTasksComplete = False
            break

    context = {"allTasksComplete": allTasksComplete, "todos": todos,
               "breadcrumb": {"parent": "Todo", "child": "Todo with database"}}

    return render_template('applications/to-do/to-do.html', **context)


@main.route('/to_do_database', methods=['POST'])
def add_todo():
    description = request.form.get('description')
    if description != '':
        new_task = Todo(description=description)
        db.session.add(new_task)
        db.session.commit()

    return redirect('/to_do_database')


@main.route('/markAllTasksComplete')
def markAllComplete():
    todos = Todo.query.all()
    for todo in todos:
        update_todo = Todo.query.filter_by(id=todo.id).first()
        update_todo.completed = True
        db.session.commit()
    return redirect('/to_do_database')


@main.route('/toggleComplete/<int:id>')
def toggleComplete(id):
    todo = Todo.query.filter_by(id=id).first()
    todo.completed = not todo.completed
    db.session.commit()
    return redirect('/to_do_database')


@main.route('/deleteTask/<int:id>')
def deleteTask(id):
    Todo.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect('/to_do_database')
