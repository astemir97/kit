var primary = localStorage.getItem("primary") || '#2243ff';
var secondary = localStorage.getItem("secondary") || '#f73164';

window.CubaAdminConfig = {
	// Theme Primary Color
	primary: primary,
	// theme secondary color
	secondary: secondary,
};
