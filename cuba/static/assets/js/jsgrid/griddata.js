'use strict';
(function() {
    var db = {
        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.Name || client.Name.indexOf(filter.Name) > -1)
                    && (!filter.Age || client.Age === filter.Age)
                    && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    && (!filter.Country || client.Country === filter.Country)
                    && (filter.Married === undefined || client.Married === filter.Married);
            });
        },
        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
        },
        updateItem: function(updatingClient) { },

        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }
    };
    window.db = db;
    db.countries = [
        { Name: "India", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 },
        { Name: "France", Id: 4 },
        { Name: "Brazil", Id: 5 },
        { Name: "China", Id: 6 },
        { Name: "Russia", Id: 7 }
    ];
    db.clients = [
        {
            "Task": "Azam",
            "Email": "Pixel@efo.com",
            "Phone": "+91 9152639845",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-warning\">In progress</span>",

            "Id": "1",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Ali",
            "Email": "Dewvrak12@gmail.com",
            "Phone": "+91 9563256895",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-warning\">In progress</span>",

            "Id": "2",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Valid",
            "Email": "Lakhsr33@gmail.com",
            "Phone": "+91 8569325641",
            "Date": "12/07/2022",
            "Status": "<span class=\"font-warning\">In progress</span>",

            "Id": "3",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Dzhamil",
            "Email": "Fatik02@gmail.com",
            "Phone": "+91 7589563241",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-danger\">Pending</span>",

            "Id": "4",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Zakhir",
            "Email": "Qmab555@gmail.com",
            "Phone": "+91 6598741235",
            "Date": "12/07/2022",
            "Status": "<span class=\"font-success\">Done</span>",

            "Id": "5",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Idris",
            "Email": "Tatypd85@gmail.com",
            "Phone": "+91 9586471230",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-success\">Done</span>",
            "Progress": "75%",

            "Id": "6",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Kurban",
            "Email": "dohamo6883@gmail.com",
            "Phone": "+91 7152849563",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-danger\">Pending</span>",

            "Id": "7",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
        {
            "Task": "Latif",
            "Email": "femecom377@gmail.com",
            "Phone": "+91 7594826315",
            "Date": "26/09/2022",
            "Status": "<span class=\"font-warning\">In progress</span>",

            "Id": "8",
            "Guest Name": "Virat Kohli",
            "Pilgim type": "Umrah",
            "Status": "Done",
            "Start date": "4:30",
        },
    ];
}());